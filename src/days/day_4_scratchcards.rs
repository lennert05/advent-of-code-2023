pub async fn scratchcards_score(body: String)-> String {
    let cards: Vec<Scratchcard> = body.lines().into_iter().map(|game_string| Scratchcard::new(game_string)).collect();
    let points_per_game: Vec<i32> = cards.iter().map(|card| card.score()).collect();
    let total_points: i32 = points_per_game.iter().sum();
    format!("{:#?}", total_points)
}

pub async fn scratchcards_played(body: String)-> String {
    let mut cards: Vec<Scratchcard> = body.lines().into_iter().map(|game_string| Scratchcard::new(game_string)).collect();
    cards.clone().iter().for_each(|card|{
        let points = card.found_numbers();
        if card.game < card.game+points {
            let copies = cards.iter().find(|game| game.game == card.game).unwrap().copies;
            for i in (card.game+1)..=(card.game+points){
                match cards.iter_mut().find(|game| game.game == i) {
                    Some(game) => game.add_copy(copies),
                    None => {}
                }
            }
        }

    });
    let sum: i32 = cards.iter().map(|game|{
        game.copies
    }
    ).sum();

    format!("{:#?}", sum)
}

#[derive(Debug, Clone)]
struct Scratchcard{
    game: i32,
    winning_numbers: Vec<i32>,
    numbers: Vec<i32>,
    copies: i32
}

impl Scratchcard{
    fn new(game_string: &str)-> Self{
        let parts = game_string.split(|char|char == ':' ||char == '|').collect::<Vec<&str>>();
        let nr: i32 = parts.first().unwrap().split_ascii_whitespace().last().unwrap().parse().unwrap();
        let winning: Vec<i32> = parts.get(1).unwrap().split_ascii_whitespace().into_iter().map(|str| str.parse::<i32>().unwrap()).collect();
        let numbers: Vec<i32> = parts.get(2).unwrap().split_ascii_whitespace().into_iter().map(|str| str.parse::<i32>().unwrap()).collect();

        Self{
            game: nr,
            winning_numbers: winning,
            numbers,
            copies: 1
        }
    }

    fn score(&self)-> i32{
        let points = self.found_numbers();
        match points {
            p if p == 1 => 1,
            p if p > 1 => 2_i32.pow((points -1) as u32),
            _ => 0
        }
    }

    fn found_numbers(&self) -> i32{
        self.numbers.iter().filter(|&number| self.winning_numbers.contains(number)).collect::<Vec<_>>().len() as i32
    }

    fn add_copy(&mut self, amount: i32){
        println!("{}+{}", self.copies, amount);
        self.copies = amount + self.copies;
    }
}