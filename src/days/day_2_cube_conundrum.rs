pub async fn cube_conundrum(body: String) -> String {
    let scores_per_game: Vec<_> = format_data(body)
        .into_iter()
        .map(|(game, score)| {
            (
                game,
                score
                    .iter()
                    .filter(|&s| s.is_valid())
                    .collect::<Vec<_>>()
                    .len()
                    == score.len(),
            )
        })
        .collect();

    let valid_games: Vec<i32> = scores_per_game
        .iter()
        .filter(|(_game, valid)| valid == &true)
        .map(|(game, _)| game.to_owned())
        .collect();

    let result: i32 = valid_games.iter().sum();
    return format!("{:#?}", result);
}

fn format_data(input: String) -> Vec<(i32, Vec<Score>)> {
    let data: Vec<(i32, Vec<&str>)> = input
        .lines()
        .into_iter()
        .map(|str| str.split_once(':').unwrap())
        .map(|(game, results)| {
            (
                game.split_whitespace().last().unwrap().parse().unwrap(),
                results.split(';').collect(),
            )
        })
        .collect();

    return data
        .iter()
        .map(|(game, results)| {
            (
                game.to_owned(),
                results
                    .iter()
                    .map(|&value| string_to_score(value))
                    .collect::<Vec<Score>>(),
            )
        })
        .collect();
}

pub async fn cube_conundrum_part_2(body: String) -> String {
    let games: Vec<Game> = format_data(body)
        .into_iter()
        .map(|(id, scores)| Game::new(id, scores))
        .collect();

    let result: i32 = games
        .into_iter()
        .map(|game: Game| game.minimal_cubes_multiplication())
        .collect::<Vec<i32>>()
        .iter()
        .sum();
    return format!("{:#?}", result);
}

fn string_to_score(str: &str) -> Score {
    let parts: Vec<_> = str
        .split(',')
        .collect::<Vec<&str>>()
        .iter()
        .map(|&str| str.trim().split_once(' ').unwrap())
        .map(|(amount, color)| (amount.parse::<i32>().unwrap(), color))
        .collect();

    Score::new(
        parts
            .iter()
            .filter(|(_, color)| color == &"red")
            .map(|(amount, _color)| amount)
            .sum(),
        parts
            .iter()
            .filter(|(_, color)| color == &"blue")
            .map(|(amount, _color)| amount)
            .sum(),
        parts
            .iter()
            .filter(|(_, color)| color == &"green")
            .map(|(amount, _color)| amount)
            .sum(),
    )
}

struct Score {
    red: i32,
    blue: i32,
    green: i32,
}

impl Score {
    pub fn new(red: i32, blue: i32, green: i32) -> Self {
        Self { red, blue, green }
    }

    pub fn is_valid(&self) -> bool {
        self.red <= 12 && self.blue <= 14 && self.green <= 13
    }
}

struct Game {
    id: i32,
    scores: Vec<Score>,
}
impl Game {
    pub fn new(id: i32, scores: Vec<Score>) -> Self {
        Self { id, scores }
    }

    pub fn minimal_cubes_multiplication(&self) -> i32 {
        let red: i32 = self
            .scores
            .iter()
            .map(|score: &Score| score.red)
            .collect::<Vec<i32>>()
            .into_iter()
            .max()
            .unwrap();
        let blue: i32 = self
            .scores
            .iter()
            .map(|score: &Score| score.blue)
            .collect::<Vec<i32>>()
            .into_iter()
            .max()
            .unwrap();
        let green: i32 = self
            .scores
            .iter()
            .map(|score: &Score| score.green)
            .collect::<Vec<i32>>()
            .into_iter()
            .max()
            .unwrap();

        red * blue * green
    }
}
