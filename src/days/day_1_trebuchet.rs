use axum::Json;
use std::collections::HashMap;
use std::fmt::format;
use std::iter::Map;
use std::str::Lines;

pub async fn trebuchet(body: String) -> String {
    let lines: Vec<&str> = body.lines().collect();
    let result = decode_vec_of_string(lines);
    return format!("{:?}", result);
}

fn decode_vec_of_string(vec: Vec<&str>) -> i32 {
    let number_lines: Vec<_> = vec
        .iter()
        .map(|&line| {
            line.chars()
                .filter(|char| char.is_ascii_digit())
                .collect::<Vec<char>>()
        })
        .collect();
    let number_sequence: Vec<i32> = number_lines
        .iter()
        .map(|vec| {
            if vec.len() > 1 {
                format!("{}{}", vec.first().unwrap(), vec.last().unwrap())
                    .parse()
                    .unwrap()
            } else {
                format!("{}{}", vec.first().unwrap(), vec.first().unwrap())
                    .parse()
                    .unwrap()
            }
        })
        .collect();

    number_sequence.iter().sum()
}

pub async fn trebuchet_part_2(body: String) -> String {
    let lines: Vec<_> = body.lines().collect();
    let number_lines: Vec<Vec<(usize, i32)>> = lines
        .into_iter()
        .map(|line| replace_to_numbers(line))
        .collect();
    let number_per_line: Vec<i32> = number_lines
        .iter()
        .map(|vec| {
            if vec.len() > 1 {
                format!("{}{}", vec.first().unwrap().1, vec.last().unwrap().1)
                    .parse()
                    .unwrap()
            } else {
                format!("{}{}", vec.first().unwrap().1, vec.first().unwrap().1)
                    .parse()
                    .unwrap()
            }
        })
        .collect();
    let result: i32 = number_per_line.iter().sum();
    return format!("{:#?}", result);
}

fn replace_to_numbers(input: &str) -> Vec<(usize, i32)> {
    let numbers_as_string = HashMap::from([
        ("one", 1),
        ("two", 2),
        ("three", 3),
        ("four", 4),
        ("five", 5),
        ("six", 6),
        ("seven", 7),
        ("eight", 8),
        ("nine", 9),
    ]);

    let matching_positions_text: Vec<_> = numbers_as_string
        .iter()
        .flat_map(|(&key, _value)| input.match_indices(key))
        .map(|(pos, value)| (pos, numbers_as_string.get(value).unwrap().to_owned()))
        .collect();

    let matching_positions_numbers: Vec<_> = input
        .match_indices(|c: char| c.is_ascii_digit())
        .map(|(pos, number)| (pos, number.parse::<i32>().unwrap()))
        .collect();
    let mut combined: Vec<(usize, i32)> =
        [matching_positions_text, matching_positions_numbers].concat();
    combined.sort_by(|a, b| a.0.cmp(&b.0));
    combined
}
