use std::ops::RangeInclusive;

pub async fn part_numbers(body: String) -> String {
    let matrix: Vec<Vec<char>> = body
        .lines()
        .into_iter()
        .map(|str| str.chars().collect())
        .collect();
    let (numbers, symbol_locations) = translated_matrix_locations(&matrix);

    let valid_numbers: Vec<_> = numbers.clone().into_iter()
        .filter(|number_location| number_location.is_valid(&symbol_locations))
        .map(|number_location| number_location.value)
        .collect();

    let sum: i32 = valid_numbers.iter().sum();



    format!("{:#?}", sum)
}

pub async fn gear_ratios(body: String) -> String{
    let matrix: Vec<Vec<char>> = body
        .lines()
        .into_iter()
        .map(|str| str.chars().collect())
        .collect();
    let (numbers, symbol_locations) = translated_matrix_locations(&matrix);

    let gear_ratios: Vec<_> = symbol_locations.iter()
        .filter(|symbol_location| symbol_location.is_gear())
        .map(|symbol_location| symbol_location.location.calculate_ratio(&numbers))
        .collect();

    let sum: i32 = gear_ratios.iter().sum();



    format!("{:#?}", sum)
}

fn translated_matrix_locations(
    matrix: &Vec<Vec<char>>,
) -> (Vec<NumberLocation>, Vec<SymbolLocation>) {
    let mut number_locations: Vec<NumberLocation> = vec![];
    let mut symbol_locations: Vec<SymbolLocation> = vec![];
    matrix.iter().enumerate().for_each(
        |(row, vec)| {
            let mut number: String = "".to_string();
            vec.iter().enumerate().for_each(|(index, char)|{
                match char {
                    x if x.is_ascii_digit() && index == vec.len()-1 =>{
                        number = format!("{number}{x}");
                        let formatted_number: i32 = number.parse().unwrap_or_else(|_|0);
                        let location = Location::new(row, index.saturating_sub(number.len()));
                        let number_location = NumberLocation::new(formatted_number, location);
                        number_locations.push(number_location);
                    }
                    x if x.is_ascii_digit() =>{
                        number = format!("{number}{x}")
                    }
                    x if !(x.is_ascii_digit() || x == &'.')=>{
                        if !number.is_empty() {
                            let formatted_number: i32 = number.parse().unwrap_or_else(|_|0);
                            let location = Location::new(row, index.saturating_sub(number.len()));
                            let number_location = NumberLocation::new(formatted_number, location);
                            number_locations.push(number_location);
                            number = "".to_string()
                        }
                        let symbol_location = SymbolLocation::new(x.clone(), Location::new(row, index));
                        symbol_locations.push(symbol_location)
                    }
                    _ => {
                        if !number.is_empty() {
                            let formatted_number: i32 = number.parse().unwrap_or_else(|_|0);
                            let location = Location::new(row, index.saturating_sub(number.len()));
                            let number_location = NumberLocation::new(formatted_number, location);
                            number_locations.push(number_location);
                            number = "".to_string()
                        }
                    }
                }
            })
        }
    );
    (number_locations, symbol_locations)

}


#[derive(Debug, Clone)]
struct NumberLocation {
    value: i32,
    row_aura: RangeInclusive<usize>,
    col_aura: RangeInclusive<usize>,
}
impl NumberLocation {
    fn new(value: i32, locations_first_digit: Location)-> Self{
        let len = value.clone().checked_ilog10().unwrap_or(0);
        let row_aura = locations_first_digit.row.saturating_sub(1)..=locations_first_digit.row.saturating_add(1);
        let col_aura = locations_first_digit.col.saturating_sub(1)..=locations_first_digit.col.saturating_add((len + 1) as usize);
        Self{
            value,
            row_aura,
            col_aura
        }
    }
    fn is_valid(&self, symbol_locations: &Vec<SymbolLocation>)-> bool{
        symbol_locations
            .iter()
            .filter(|&location| self.row_aura.contains(&location.location.row) && self.col_aura.contains(&location.location.col))
            .collect::<Vec<_>>().len() > 0
    }
}

#[derive(Debug, Clone)]
struct SymbolLocation {
    value: char,
    location: Location,
}
impl SymbolLocation{
    fn new(value: char, location: Location)-> Self{
        Self{
            value,
            location
        }
    }
    fn is_gear(&self)->bool{
        self.value == '*'
    }
}


#[derive(Debug, Clone)]
struct Location{
    row: usize,
    col: usize
}

impl Location{
    fn new(row: usize, col: usize) -> Self{
        Self{
            row,
            col: col.to_owned(),
        }
    }

    fn calculate_ratio(&self, numbers: &Vec<NumberLocation>)-> i32{
        let part_numbers = numbers
            .iter()
            .filter(|&number_location| number_location.row_aura.contains(&self.row) && number_location.col_aura.contains(&self.col))
            .collect::<Vec<&NumberLocation>>();
        if part_numbers.len() == 2 {
            part_numbers.iter().map(|&number_location| number_location.value).collect::<Vec<i32>>().iter().product()
        }else {
            0
        }
    }
}

