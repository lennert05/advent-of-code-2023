use crate::days::day_1_trebuchet::{trebuchet, trebuchet_part_2};
use crate::days::day_2_cube_conundrum::{cube_conundrum, cube_conundrum_part_2};
use crate::days::day_3_gear_ratios::{gear_ratios, part_numbers};
use axum::routing::post;
use axum::Router;
use crate::days::day_4_scratchcards::{scratchcards_played, scratchcards_score};

pub fn router() -> Router {
    Router::new()
        .route("/day/1", post(trebuchet))
        .route("/day/1/2", post(trebuchet_part_2))
        .route("/day/2", post(cube_conundrum))
        .route("/day/2/2", post(cube_conundrum_part_2))
        .route("/day/3", post(part_numbers))
        .route("/day/3/2", post(gear_ratios))
        .route("/day/4", post(scratchcards_score))
        .route("/day/4/2", post(scratchcards_played))
}
